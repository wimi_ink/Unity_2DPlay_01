﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    [System.Serializable]
    public class LevelGames
    {
        //血量
        public int Life;
        //樱桃数
        public int Cherry;
        //获得的钻石数
        public int GetDiamonds;
    }
    public LevelGames LevelGame;

    void Start()
    {
        //DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        
    }
}

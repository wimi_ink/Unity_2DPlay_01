﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public GameObject Play;
    public enum Directions
    {
        上 = 0,
        左 = 1,
        右 = 2
    }
    public Directions Direction;

    public void OnPointerDown(PointerEventData eventData)
    {
        switch (Direction)
        {
            case Directions.上:
                //Play.GetComponent<Play>().State.Top = true;
                break;
            case Directions.左:
                Play.GetComponent<Play>().State.Left = true;
                break;
            case Directions.右:
                Play.GetComponent<Play>().State.Right = true;
                break;
            default:
                Debug.Log("未选择");
                break;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        switch (Direction)
        {
            case Directions.上:
                Play.GetComponent<Play>().State.Top = true;
                break;
            case Directions.左:
                Play.GetComponent<Play>().State.Left = false;
                break;
            case Directions.右:
                Play.GetComponent<Play>().State.Right = false;
                break;
            default:
                Debug.Log("未选择");
                break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stop : MonoBehaviour
{
    public GameObject UIGame;
    public GameObject UIStop;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StopGame()
    {
        Time.timeScale = 0;
        UIStop.SetActive(true);
        UIGame.SetActive(false);
    }

    public void StartGame()
    {
        Time.timeScale = 1f;
        UIStop.SetActive(false);
        UIGame.SetActive(true);
    }

    public void OutGame()
    {
        SceneManager.LoadScene("Scenes/Interface");
    }
}

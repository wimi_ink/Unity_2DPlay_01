﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    Rigidbody2D rigidbody;


    public enum WayMovings//y移动方向
    {
        直线移动 = 0,
        跳跃移动 = 1
    }
    public WayMovings WayMoving;
    
    #region 直线移动设置
    //移动状态
    bool Moving_State;
    //移动速度 
    [Header("直线移动")]
    [Tooltip("移动速度（float）")]
    public float Speed;
    //移动时间
    [Tooltip("移动时间（float）")]
    public float Moved_Time;
    //停留时间
    [Tooltip("停留时间（float）")]
    public float Stop_Time;
    public enum Direction//移动方向（float）
    {
        上 = 0,
        下 = 1,
        左 = 2,
        右 = 3
    }
    [Tooltip("移动方向（float）")]
    public Direction Choice;
    #endregion

    #region 跳跃移动设置
    //停留的时间
    float Pause_time;
    //最后移动的时间
    float Moving_time;
    //跳跃距离
    [Header("跳跃移动")]
    [Tooltip("跳跃距离（float）")]
    public float JumpingDistance;
    //跳跃高度
    [Tooltip("跳跃高度（float）")]
    public float JumpingHeight;
    //停顿时间
    [Tooltip("停顿时间（float）")]
    public float PauseTime;
    //最大跳跃数
    [Tooltip("最大跳跃数（int）")]
    public int MaxJumpingNumber;

    ///状态
    //触地
    bool Ground;
    //空中
    bool Air;
    //上升 1 //下降 -1 // 默认 0
    int JumpingStatus;
    //上一次跳跃的时间
    float JumpTime;
    //跳跃数
    int JumpingNumber;

    //绑定
    Animator animator;
    Transform transform;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        rigidbody = gameObject.GetComponent<Rigidbody2D>();
        Moving_time = Time.time;
        animator = gameObject.GetComponent<Animator>();
        transform = gameObject.GetComponent<Transform>();
        JumpTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (WayMoving.ToString() == "直线移动")
        {
            
        }
        if (WayMoving.ToString() == "跳跃移动")
        {

            JumpAnimationSwitch();
        }
    }

    void FixedUpdate()
    {
        if(WayMoving.ToString() == "直线移动")
        {
            StraightLineMobile();
        }
        if (WayMoving.ToString() == "跳跃移动")
        {
            JumpMobile();
        }
    }

    #region 直线移动
    void StraightLineMobile()
    {
        if (Moving_State)
        {
            if (Time.time - Pause_time > Stop_Time)
            {

                Moving_State = false;
                Moving_time = Time.time;
                switch (Choice.ToString())
                {
                    case "上":
                        Choice = Direction.下;
                        break;
                    case "下":
                        Choice = Direction.上;
                        break;
                    case "左":
                        Choice = Direction.右;
                        break;
                    case "右":
                        Choice = Direction.左;
                        break;

                }
            }
            return;
        }
        switch (Choice.ToString())
        {
            case "上":
                transform.position = new Vector2(transform.position.x, transform.position.y + Speed * Time.deltaTime * 1);
                break;
            case "下":
                transform.position = new Vector2(transform.position.x, transform.position.y + Speed * Time.deltaTime * -1);
                break;
            case "左":
                transform.position = new Vector2(transform.position.x + Speed * Time.deltaTime * -1, transform.position.y);
                transform.localScale = new Vector2(1, transform.localScale.y);
                break;
            case "右":
                transform.position = new Vector2(transform.position.x + Speed * Time.deltaTime * 1, transform.position.y);
                transform.localScale = new Vector2(-1, transform.localScale.y);
                break;
        }
        if (Time.time - Moving_time > Moved_Time)
        {
            Moving_State = true;
            Pause_time = Time.time;
        }
    }
    #endregion

    #region 跳跃移动
    void JumpMobile()
    {
        //判断时间
        if (Time.time - JumpTime >= PauseTime)
        {
            //方向
            if (JumpingNumber == MaxJumpingNumber)
            {
                switch (transform.localScale.x)
                {
                    case 1:
                        transform.localScale = new Vector2(-1, transform.localScale.y);
                        break;
                    case -1:
                        transform.localScale = new Vector2(1, transform.localScale.y);
                        break;
                }
                JumpingNumber = 0;
            }
            //跳跃
            rigidbody.velocity = new Vector2(JumpingDistance * transform.localScale.x * -1, JumpingHeight);
            JumpTime = Time.time;
            JumpingNumber++;
            Air = true;
        }
    }
    #endregion

    #region 跳跃动画切换
    void JumpAnimationSwitch()
    {
        if (rigidbody.velocity.y > 0)
        {
            JumpingStatus = 1;
        }
        if (rigidbody.velocity.y < 0)
        {
            JumpingStatus = -1;
        }
        if (rigidbody.velocity.y == 0 || !Air)
        {
            Air = false;
            JumpingStatus = 0;
        }
        animator.SetInteger("JumpingStatus", JumpingStatus);
    }
    #endregion
}

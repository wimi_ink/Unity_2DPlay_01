﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{
    public string GateExit = "Scenes/Level/";
    public GameObject Game;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(Game.layer == collision.gameObject.layer)
        {
            SceneManager.LoadScene(GateExit);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public AnimatorOverrideController Play1;

    public AnimatorOverrideController Play2;

    public GameObject Play;

    int PlayID;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Switch()
    {
        switch (PlayID)
        {
            case 0:
                Play.GetComponent<Animator>().runtimeAnimatorController = Play2;
                PlayID = 1;
                break;
            case 1:
                Play.GetComponent<Animator>().runtimeAnimatorController = Play1;
                PlayID = 0;
                break;
        }
    }
}
